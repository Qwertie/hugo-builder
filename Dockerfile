FROM alpine:3.4
MAINTAINER Luke Picciau <git@itscode.red>

ENV HUGO_VERSION=0.37.1 \
    HUGO_CHECKSUM=f7b57c4d9e406719e41c84a4a70d6b332826bf356a15615ed02a450134796f81

#RUN set -ex && \
RUN apk add --no-cache openssl openssh-client git

RUN wget -q https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz
RUN tar xf hugo_${HUGO_VERSION}_Linux-64bit.tar.gz 
RUN mv hugo /usr/bin/hugo && \
    rm -f hugo_${HUGO_VERSION}_linux_amd64.tar.gz

CMD ["hugo"]
